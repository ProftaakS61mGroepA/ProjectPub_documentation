# Ordering

### Ordering tab
- Users can view all available products
- Users can create new orders
- Users can add consumptions to orders
- Users can confirm orders: the order will now be available for making
### Making tab
- Users can request next makeable order
- Users can mark makeable order as completed: the order will now be available for delivering
### Delivering tab
- Users can request next deliverable order
- Users can mark deliverable order as delivered
### Payment tab
- Users can view all orders ready for payment
- Users can confirm payment for orders

# Stock

### REST Endpoints
- Request all products  
    `stock/products`
- Request a specific product  
    `stock/products/{productId}`
### Angular Frontend
- Users can create new products.
- Users can see all products in the system.
    - Users can filter the list of products on name and/or category.
- Users can see product details
- Users can delete a product

# Drive-in
- Products can be added
- Orders can be created

### REST Endpoints
- Request all products  
    `drive-in/onorderstatuschange`