**25-09-2017**

Nieuwe onderdelen:
=======================
Schaalbaarheid:
- Product cataloog realisticher
	- denk aan 1000 producten
	- Inventarizatie lijst (excel)
	- Zie: https://trello.com/c/1ehLTFzM/28-create-modify-products-from-excel
- Senario-test / performance-test
	- BV: 5 cafees, 4 drivins, 2 klanten per timeframe
	- Nog niet optimalizeren maar hoe werkt het huidige systeem in de test senarios

Functionaliteiten:
- Communicatie tussen systemen (Stock, ordering, drive-in)
- Security
	- Java EE 8 heeft een nieuwe security API
	- Let op omzetten naar Java EE 7
	- Deze sprint:
		- Verschillende rollen
		- Test accounts

Vorm:
- Demo documenteren
	- BV: screencaptures

Notities Ordering:
===============
- Real time orders
- Aparte pagina voor keuken, bar, serveersters, etc
- Deployment raport is ingeleverd. In communicatie met Jip moet worden uitgeplaned wanneer dit gebeurd

Notities Jip:
==========
- Frontend maken mag niet vanuit Merel.
- Continiuous deployment mooier en beter uitwerken mag niet vanuit Merel.
- Overleg taken Jip moeten nog gedaan worden samen met Merel en Frank.

Nieuwe casus uitschrijven met focus op data?  
Auto deployment mag niet gedaan worden door Jip.  
Eventueel door Maiko.  

Voorstel maken op basis van notities.  
Mogelijke deadline: 25-09-2017 17:00  