Casus:
The cafe has decided to serve the more hasty customers. They modified the building to allow for a drive-in. Also they have made deals(menus) so they can lure new customers. If everything goes alright, they may even launch new franchises in the future!

Ticking tasks will be done with the following signs: ✔️ and ❌

Base requirements
*  The solution will use multiple enterprise oriented microservices.
   *  ❌ Completed
*  The customer interaction is touch screen compatible
   *  ❌ Completed
*  The system will be easy to use
   *  ❌ Completed
*  The system will hook up to the existing infrastructure/applications
   *  The bartender and cook system for optimal order completion will be used
   *  ❌ Completed
*  For every functionality, tests will be written
   *  ❌ Completed
*  The backend can exchange all relevant database data in JSON format via a REST interface
   *  ❌ Completed
*  Relevant data will be stored in the backend database
   *  ❌ Completed
*  Persistent framework for database mapping will be used
   *  ❌ Completed
*  --------------------
*  Food can be ordered
   *  ❌ Completed
   *  ✔️ In sprint backlog
*  Drinks can be ordered
   *  ❌ Completed
   *  ✔️ In sprint backlog
*  Complete menus can be ordered
   *  ❌ Completed
   *  ✔️ In sprint backlog
*  Menus can be composed by the manager
   *  ❌ Completed
   *  ❌ In sprint backlog
*  An order can be confirmed
   *  ❌ Completed
   *  ❌ In sprint backlog
*  An order can be paid
   *  ❌ Completed
   *  ❌ In sprint backlog

Extended requirements
*  The order part, speech and payments each will use their own microservice
   *  ❌ Completed
*  The backend will be hosted virtualized
   *  ❌ Completed
*  Load balancing with redundancy can be used for the microservices
   *  ❌ Completed
*  Multiple franchises can be monitored via a monitor interface
   *  ❌ Completed
*  --------------------
*  Help button
   *  A customer can press a help button. This will allow for direct speech with a member of the cafe
   *  ❌ Completed
   *  ❌ In sprint backlog