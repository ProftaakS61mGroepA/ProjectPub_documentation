# Definition of done:

## Before Reviewing
The following steps should be done before userstory on the scrumboard can be placed in "Reviewing".

1. Code produced (all specified functionality implemented)
2. Builds without errors
3. Unit tests written and passing
4. Any build/deployment/configuration changes implemented/documented/communicated
5. Relevant documentation/diagrams produced and/or updated

## Before Done
The following steps should be done before a userstory on the scrumboard can be placed in "Done".

6. Peer reviewed by another teammember (via merge request)
7. Remaining hours for Trello story set to zero