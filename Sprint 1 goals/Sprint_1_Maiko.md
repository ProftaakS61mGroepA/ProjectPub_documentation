## Proposal for stories implemented in sprint 1:

The following userstories will be implemented in sprint 1.  
Since this is the first sprint this will mean the setup of all application layers (DAO, Service and Controller), database and frontend (Angular4).  
Most likely these User Stories will be implemented top to bottom as in this document.

In these user stories there are two posible users:  
* An employee: A person working at a company that uses this application.
* A developer: A person developing an application that uses data from this application.

### User Stories

**Create new product**

> An employee should be able to add a new product when a new producted is added to their menu.

**See all products**

> An employee should be able to see all products currently in the system.

**See specific details about a product**

> An employee should be able to see specific details about a product.

**Modify existing product**

> An employee should be able to modify a existing product.

**Delete existing product**

> An employee should be able to delete a existing product when this product is no longer available at their business.

**Set products in stock**

> An employee should be able to set the amount of stock that is currently pressent in their inventory.

**Set minimum products in stock**

> An employee should be able to set the minimum amount of stock there has to be in their inventory for a specific product.

**Request all products**

> A developer should be able to request a list of all products

**Request a specific product**

> A developer should be able to request a specific product

**Check product availability**

> A developer should be able to check de availability of a product for use in his own application.

**Reserve a product**

> A developer should be able to reserve a product for use in his own application.

**Use a product**

> A developer should be able to signal that a reserved product has been used in his application.