## Proposal for stories implemented in sprint 1:

**Add consumption to order**

> To be able to serve order to customers, the server can add comsumptions to an order.

**Mark order as makeable**

> To signal to cooks / barmen that orders can be made, the server can mark an order as ready to make.

**Mark order as deliverable**

> To signal to servers that an order can be delivered, cooks / barmen can mark an order as ready to deliver.

**Process order payment**

> To close an order, servers can notify the system that an order is paid for.

**Request next makeable order: any**

> To know what items should be prepared, cooks and barmen can request the next makeable order.

**Request next makeable order: specific**

> To know what items they should prepare, a cook or barman can request makeable orders relevant to them.

**Request next deliverable order**

> To know what items can now be delivered, servers can request the next deliverable order