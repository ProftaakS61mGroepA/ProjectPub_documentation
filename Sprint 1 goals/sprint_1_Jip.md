## Proposal for sprint 1:

The following userstories will be implemented in sprint 1.  
Since this is the first sprint this will mean the setup of all application layers (DAO, Service and Controller), database and frontend (Angular4).  
Most likely these User Stories will be implemented top to bottom as in this document.

In these user stories there is one possible users:  
* A costumer: A person touching the touch interface from the drive-in.

### User Stories

**Food can be ordered**

> With a touch interface, a costumer can order food

**Drinks can be ordered**

> With a touch interface, a costumer can order drinks

**Complete menus can be ordered**

> Predefined or mock menus can be chosen by the costumer when ordering
