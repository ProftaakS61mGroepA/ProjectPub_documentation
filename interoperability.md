# The idea behind this document is to have a clear visualisation of the available interfaces, url and ports per system.

## Supplier
* Ports:
  * Backend with JSF
    * 10031

## Stock
* Ports:
  * Web interface
    *  10020
  * REST interface
    * 10021
  * database
    * 10022
* REST endpoints
    * /stock/products
      * Method: GET
      * In: -
      * Out: product[]
    * /stock/products/{id}
      * Method: GET
      * In: -
      * Out: product
    * /stock/products/updated
      * Method: POST
      * In: product[]
      * Out: product[]
    * /stock/products/reserve
      * Method: PUT
      * In: product[]
      * Out: product[]
    * /stock/products/use
      * Method: PUT
      * In: product[]
      * Out: product[]

## Ordering
* Ports
  * Web interface
    *  10010
  * REST interface
    * 10011
  * database
    * 10012
* REST endpoints
    * /ordering/create
      * Method: POST
      * In: productOrder
      * Out: productOrder
    * /ordering/update
      * Method: PUT
      * In: productOrder
      * Out: productOrder
    * /ordering/fulfill
      * Method: PUT
      * In: productOrder
      * Out: productOrder
    * /ordering/complete
      * Method: PUT
      * In: productOrder
      * Out: -
    * /ordering/abort
      * Method: PUT
      * In: productOrder
      * Out: -

## Drive-in
* Ports
  * Web interface
    *  10000
  * REST interface
    * 10001
  * database
    * 10002
* REST endpoints
    * /drive-in/onorderstatuschange
      * Method: PUT
      * In: productOrder
      * Out: -

* JSON Models:  

**product**
```
{
    "id": number,
    "name": string,
    "priceInCents": number,
    "category",(    
        FOOD,//e.g some fries
        DRINK,//e.g. a cola
        OTHER,//e.g. a straw)
}
```

**productOrder**
```
{
    "id": number,
    "products": product[],
    "orderState": string,(    
        CREATED,//Order received by ordering backend, id assigned
        MAKEABLE,//Order in queue for bar/kitchen
        MAKING,//Kitchen/bar working on order
        DELIVERABLE,//Order can be delivered
        DELIVERING,//Order is being delivered
        DELIVERED,//Order is delivered
        COMPLETED,//Order is paid and done
        ABORTED;//Order is aborted before payment)
    "origin", string(DRIVEIN,ORDERING)
}
```