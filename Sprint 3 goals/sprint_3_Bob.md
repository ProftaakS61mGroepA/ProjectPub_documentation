## Proposal for stories implemented in Sprint 3

**Role-based availability**

> To better control functionality usage, users can now only view and access functionality relevant to their role.

**Performance optimization**

> To improve responsiveness, performance should be improved in bottlenecks that were discovered during stress testing.

**Realtime updates from scaled servers**

> Scaling the back end server breaks realtime updates. These features should co-exist.

