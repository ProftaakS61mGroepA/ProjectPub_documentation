## Project Pub Documentation

All documentation for the various aspects of this project should be placed in this repository.
The preferred file format for text documents is Markdown.

[PlantUML](http://plantuml.com/) can be used for diagrams.