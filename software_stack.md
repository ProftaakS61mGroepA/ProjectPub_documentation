# Software Stack Project Pub

## Front End: Angular 4

Reasoning: an important aspect of the project is loose coupling between components. When it comes to front end <-> back end communication, three things can be done to decouple these components at a design level:
- Make the interface / transport layer as generic as possible.
- Use different technologies for both.
- Make the front end purely responsible for user interaction.

Using Angular as a front end accomplishes the first item by allowing us to use a REST interface (HTTP requests are about as stable and generic as it gets).

Angular almost automatically ensures the second point as well, as TypeScript will use a separate build stack.

## Back End: default=java

Requirements state that all-but-one of the back end applications should be implemented using Java EE.

## Database: default=PostgreSQL

The requirements for the database are:
- Ease of use
- Ease of setup (also in a local development environment)
- Compatibility
- Freeware

PostgreSQL matches all these criteria.

## Component Interfacing: REST

The default interfacing between components is a REST interface.
This is the most stable, generic, and widely supported interprocess communication interface.

## Documentation: Markdown

Compared with various Office documents, Markdown has multiple advantages:
- Layout elements are declared in syntax
- Based on plain text => can be managed in Git
- Can easily be compiled to PDF documents
- Easy to learn