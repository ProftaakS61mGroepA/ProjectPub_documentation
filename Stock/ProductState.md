# Product States

Using the REST endpoints available on the stockbackend another application is able to check the availability, reserve and use products. For more information about these rest endpoints see [interoperability.md](interoperability.md).

In this document the different states of a Product are described. This is relevant because a product needs to be in stock to be used. In order to use a product the product must be reserved so two different clients won't try to use the same product at the same time.  
Each product should run through the following states before it can be used.   
![](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuUK2iafI5GAAybDAaqiKgdcv51GWGa6fnQb5PQaf4345XMYjM0NTbFpoF5qx1SUonCoSnAISL8WKdAkWMvIPdb42L05IRQKGN9gSd5fSZ6G0JDullmJD46GHk4KGOzo0mgaf86Mr809i450Ixa8h61Fc3v0gkAmkn1nUBg1cXzIy5A2t0G00 "ProductReserveDiagram@1-12")
![](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuUK2iafI5GAAybDAaqiKgdcv51GWGg75gIaGle92j5QiWcvAVdcUhfr2K6fnQb5PQWf41iPJq2tAJCye0Sg3r5ef19Sc9wSM5oDfXydxvPUW6e4a0BC3OePROcOEH54jY02R0fG4EymAHC6S3P85Y-f2zF9o0JKufEQb07q80000 "ProductUseDiagram@1-12")  
`/stock/products/reserve` and `/stock/products/use`  

To prevent to many REST requests the api endpoints for availability, reserve and use accept a list of products. So when you want to reserve products all products in that request must be available and when using products all products must be reserved. So when one product is not available/reserved none of the products in that request will get a state change.  
To correctly use a product the client should use the following sequence diagram:  
![](http://www.plantuml.com/plantuml/png/pLEzJiCm4Duj-Htk1L1s1bIfCB2XLfs0uPfSr5373kTpnUDppO-L394wK8dDIUxxko_UH46Mr3fhi9J0i7KDUR6c35uuEDXBAC_OTTPKA7Z4IF0y7gmPXk7XAO_18nJnUYuw3bKg9HQfKoHL7_uh1Wzlm-ltJsioHl4Jf-8F_Jp8YVHnOh4cJypKbXoJzPjSiRBpXUxlQkPXUwBo34qDw5pc06I2mGqU7It0FsCcnRvw8VQeXLnjCxmm1rvLsWtfUY6cxqHH6vZPNWcvHyKupHa5L2GsmKS8zGemUzkH9FPZY8NERJz8_5yrKFLN9RJXahkLVfdezIBmy66KXff32_rExbICBdXUoePyfJV-5m00 "ProductSequenceDiagram@1-25")  

The application assumes the following cases can occur:
* An order can contain duplicate products.
* An order can contain products that are not up-to-date with the database. And will always return the most up-to-date version from the database.
