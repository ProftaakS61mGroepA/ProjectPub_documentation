# Security

## Proposal
To implement security in the ProjectPub application we would like to investigate the posibility of oAuth2. oAuth2 is a widely used security feature in wich you connect to another user database for checking the authenticety of a user. This also means knowing oAuth2 is a valuable skill for us in the future. Apart from that, oAuth2 makes signing in easier for end users. End users can now log in to our application using their pre-existing google account.

Finally oAuth2 removes a large security concern for our application. We can outsource authenticating users to proven and secure google servers. We no longer have to worry about keeping sensitive information safe, such as mailadresses and passwords.

## Implementation
For our oAuth2 endpoint we chose google. This is because google supplied the best documentation, a [playground](https://developers.google.com/oauthplayground/) for testing and learning and a [library](https://developers.google.com/api-client-library/java/).
On their page [Using OAuth 2.0 to Access Google APIs](https://developers.google.com/identity/protocols/OAuth2#device) is explaned how oAuth works and which scenarios googles endpoint supports. We will be using the "Web Server Applications" scenario but the "Service Accounts" scenario might be useful in the future when authenticating requests made by backend services. For example: when our ordering backend requests all products from stock.

Below a sequence diagram of how oAuth2 is implemented in the ProjectPub project.  
![oAuth2 Protocol in ProjectPub](http://www.plantuml.com/plantuml/png/TPDFRu904CNl97o7gOVUAacF9csq_qnFDMflNjPbg5jnnymEqlJJTmG4BIq70Zk_UU-nEuo369PYbyUHiK8CWndJ9rgP5UkLVs7oWZuTW0cG7L_YoEgTARy47upTdhYrFiTHQiIiJS1BxgbPhS0JSgOv9Vh8SUKNo0VaK64VrLeSnL6L06xkAmiOmVHv2KaGijl4M4k5bn17LKsP6Xv1HbmQJfSqRNHQbLfOaSb-ptH0w9B6KLCy4vfZwbZBjRlG6GwbamrC2jaGksyZZlmZfTXFF7jRz2Al_7_Tfq_fWlQPBcTZCR4MGu1hpPinXWqiIO_c3x4v2ZlzwlU0_EepMlcDo3wCagGios6DcRqBGqkxf538APGSRfDTpxPJRuzTJmS5Zcrdiz3Y9gzhS7K7-EM2pXNezOVAmZGHQw8lfMFNsPy6GwsFFO-CANfn9W-rvw_5V-mRbTRwGkR2hviDATYtMwcxtnn3i_bZxTAVx0S0 "oAuth2 Protocol in ProjectPub")  

### How to implement
Google Supplies a number of endpoints which can be tested in their oAuth playground. For us the basic endpoint https://www.googleapis.com/auth/plus.login will be enough. Before we can start using this endpoint we have to register our application with the [Google API Console](https://console.developers.google.com/) from which we will then get a `ClientID` and `ClientSecret`. See "Create Google Project" on how to do this.  
After this we can start using the endpoint by forwarding our users to the url with our `ClientID` in it. When a user autherizes our application google will send us the AuthorizationCode for that user.  
We can exchange this AuthorizationCode for a Refresh Token and Authentication Token using our `ClientID` and `ClientSecret`. This will be returned by google in JSON like this:  
```
{
  "access_token": access_token, 
  "token_type": "Bearer", 
  "expires_in": 3600, 
  "refresh_token": refresh_token, 
  "id_token": id_token
}
```   
We can then start using the Access Token to request user data from google. For example we can send a request to https://www.googleapis.com/oauth2/v2/userinfo with the Access Token. This URL will return the following userinfo in a JSON format:
```
{
  "family_name": family_name, 
  "name": name, 
  "picture": picture, 
  "locale": locale, 
  "link": "https://plus.google.com/{userId}", 
  "given_name": given_name, 
  "id": id
}
```

### Create Google Project
- Create a new Google Project on the [Google API Console](https://console.developers.google.com/projectselector/apis/library)
- Enter a ProjectName under Credentials > OAuth-PermissionScreen
- Create new credentials under Credentials > Credentials
	- When creating a JavaEE (or other serverside webapps) you choose: Client-ID OAuth > Webapp
	- Then enter a name and redirectUrl.
- After this you can download `client_secrets.json`. You will need this file in you're JavaEE application.
- Go to you're dashboard and activate "Google+ API".

### What to save
If we follow the procedure defined above we will only have to save the GoogleId in our database to define users. We can then define user roles and connect these to GoogleId's in our database.

### Dependencies:
```
<!-- https://mvnrepository.com/artifact/com.google.code.gson/gson -->
<dependency>
    <groupId>com.google.code.gson</groupId>
    <artifactId>gson</artifactId>
    <version>2.8.2</version>
</dependency>

<!-- https://mvnrepository.com/artifact/com.google.api-client/google-api-client -->
<dependency>
    <groupId>com.google.api-client</groupId>
    <artifactId>google-api-client</artifactId>
    <version>1.23.0</version>
</dependency>

<!-- https://mvnrepository.com/artifact/com.google.oauth-client/google-oauth-client -->
<dependency>
    <groupId>com.google.oauth-client</groupId>
    <artifactId>google-oauth-client</artifactId>
    <version>1.23.0</version>
</dependency>

<!-- https://mvnrepository.com/artifact/com.google.oauth-client/google-oauth-client -->
<dependency>
    <groupId>com.google.oauth-client</groupId>
    <artifactId>google-oauth-client-servlet</artifactId>
    <version>1.23.0</version>
</dependency>
```