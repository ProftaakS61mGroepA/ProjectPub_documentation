# Proposal Proftaak extension research/prototype projects

During S6 a set of technical, professional, and personal competencies should be proven by the student.

The overall goal of these spikes is to make efficient use of the limited time available during S6, while still proving all required competencies.

The concrete proposal is to implement multiple research projects aimed at integrating new functional or nonfunctional features with the proftaak project. These features should fall within the scope of the S6 curriculum, but can and should add further depth to the subject.

The following set of research topics are proposals. Not all of them have to be implemented, and they can be implemented in any order.

This set of possible topics should be changed and appended based on tutor feedback, and the requirements of the proftaak.

## Automated scalability testing

Scalability is already a nonfunctional requirement of the proftaak application, but it is a secondary point. For all intents and purposes, it working in theory is sufficient.

This can be improved upon by specific research into what can be done to automatically demonstrate this - along with any changes based on the findings.

## Implementation of optimized realtime update systems for many users

Using messaging mechanisms, it is possible to push updates to specific user interface sessions.
Pushing everything to everyone is simple. Determining who needs the message (which server is closest and not busy?), and updating them - with fallback plans - becomes more complex. 
Doing so when the application is spread over multiple servers becomes an interesting challenge.

## Distributed database systems

In a microservice architecture, should the database be maintained centrally? Should every service keep its own database, and sync required data? How can data redundancy be handled over multiple systems?

## One-click server deployment

A possible scenario: the system for a newly opened mega-restaurant is struggling under its load during rush hour, but idling otherwise.

What if deployment of more servers was as simple as pushing a slider? What if it could add or remove new services based on how busy the restaurant is?