# Synchronizing pushed updates

When simultaneously implementing a load balancer, and websocket updates, there is a problem: updates are not synchronized.

Websocket pushes are triggered by ORM events, which in turn are only triggered on the backend that made the actual change. Any other backends that were spawned by scaling the docker image are not triggered.

Migrating the event to the database, where it would communicate with all backends was investigated. It is technically possible, but not advisable.

A better solution is to implement a message broker where events are published by the backend that made the update. All concurrent backends can subscribe to this broker, and forward the update event to their connected websocket clients.

This solution allows both websocket push events, and scaled applications. It still distributes websocket connections over all backend instances, reducing the load on any single container.

![publish_relay](http://www.plantuml.com/plantuml/png/ZPCzRyCW48PtI_uFA5sIeQfiRncgA6exjLA7PgewWBcuA1GZFlBx2oO16yjHDixtyDvxTtYd3LR6_l6oaEx0MYQnC6ZrfdfXG51KhH3MwEZ3Qecf4rClCKrYcenvxp55UyonQ44D6Ba46RZ7xIbv8Ywg5eWw4hcX3z0QTu28wa_NQiDnohrWWmdMuFEKb4LPX16Wn--7JW68d-VeSekq9RfLZ41PN3zEGUTqGeLPUOPm2_TeHSwrD9TgF2AjalmkiH6PEqkDZdK-3r-eLnGK4ztJaGccVobmTlPZIy9HxpRk4CzIiaLMKcnWFR2Rig1a9DABmpGRRuQ5RP6qX3lfTL1vFOCm6zz1o4-aXcG6NlJIdaBdCrUHYsCBCv-1UUNwLkNnziDpcEdjt5Dsl-O_ "publish_relay")

In this scenario, there are three frontend clients active, and two backend servers.

On startup, the clients subscribe to websocket updates. These requests are distributed by the load balancer - Frontend 1 and 3 are handled by Backend 1, and Frontend 2 is handled by Backend 2.

Afterwards, Frontend 1 makes a HTTP request that triggers an order update in the database. This request is handled by Backend 1.

After Backend 1 commits the database transaction, the ORM calls the onUpdate() event handler. This handler creates an event message, and publishes it to the message broker.

Both backends are subscribed to this message broker, and receive the event message. They forward this event to all subscribed websocket clients.
