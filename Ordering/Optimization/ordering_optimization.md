# Ordering optimization analysis

## Introduction

This document seeks to explore possible venues of optimization in the Ordering API. 

It must be noted that this analysis is not yet based on known bottlenecks. Stress testing of the API is currently in progress. 

The goal of any optimization would be to improve responsiveness and scalability of the API. HTTP requests should respond quickly, and continue to do so when many clients are interacting with the API.

## Directions

Optimization can be done in two directions: improve response time, and improve horizontal scalability.
Both approaches influence each other. 

The response time is improved by reducing the time taken by the back end to handle an individual request. The first few users get the maximum benefit, while other concurrent users might have to wait in a queue before their request is quickly handled.

Horizontal scalability ensures that even when many users are connecting simultaneously, their individual response time remains stable.

While this does not directly increase response time, it will start doing so when it prevents or shortens any queues in which the request has to wait before getting handled.

Requests that make external calls (databae, other API, file I/O) are prime candidates for improving response time. Short requests that are sent often by the GUI should scale well horizontally.

## Case 1: GUI startup
![GUI startup](http://www.plantuml.com/plantuml/png/bPGnRuCm48LtI_uF9xgq0scxCaHDcoZArAX9fgg3mHQoue1bJ9J--zh449iuaSe4kkyUxvuFtXfDb6uF0fQx5PmV94RDdbUIA7A0z_gq9fJogeJN5ummcXMwLf0iAWgxXga4I0EjkS78D6XUS4agRTKidDSdI3uLPSeAvAJO0wjeWX6Oo-UJhyLc2xFrgjCZabyZ6wQEl60O6GKmmeF-HjT6zuvu1nZjwzR1sfneafE6HRgfAmNTTc98ftREx18RHZR5zFkXL8nLFrCxI0RBnHQagcbRwEQP235GkMXP52g8PcMjE0jvsIefWexQ-RSOPO8VcMBKyTFkM3AWl962_9gPIZF3P3B1oFJxBbso6PHCpuHOEnkFJnYvKa2xD6E3zRKBdtho4NuidxhXhfq4dllaRqRRcnr5u53WLIGETnoZD3w6inZbqU6Xtv3rPQE6Rnopyn5Qlx1nyyDkZEpF-wqOkH_uVhsZ3NtHwqZ_-uemeW0ElViPtLjiptoOaM5XWCyINbeOlPaFsFpg_W00 "GUI startup")

When loading the Ordering application, the base page is displayed immediately, and multiple requests for data are sent to the REST API.
The REST API handles the requests asynchronously, and as those requests are answered, the GUI updates itself with newly received data.

Frontend-wise, there is no need for optimization here. All data is displayed as quickly as possible, with separate components not being blocked by other (slower) data requests.

In the backend, focus should lie on improving response times. This set of requests is made at application startup, not continually throughout the session. 

### Improving getAllProducts()
This request is forwarded to the Stock API, who may or may not take a long time answering.

We can't simply return as soon as we've sent the request: we want to reply with the response data.
We can, however, offload making the request to Stock to a different thread. This frees up the API thread to handle other requests while we're waiting for Stock to answer.

The taken approach (querying a different API) also means we are deserializing and serializing the data within our application. If the data returned from Stock is guaranteed to be valid for consumption by the front end, we might want to skip this step.

Possible changes:

- Freeing up the API thread by suspending the thread while waiting.
- directly forwarding the response to Stock, saving a deserialization/serialization cycle, and HTTP overhead.
- dividing item requests in pages: only requesting new item pages when user scrolls down or searches.

### Improving getAllCategories()
This is a relatively simple request: no outside data is required.
Any further optimization could involve not asking the back end at all, but making the categories available in the front end.

This would require categories to be deploy-time stable: front end configuration would need to be updated for every change.

Possible changes:

- Adding categories to front end configuration, eliminating the need for a back end round trip.

### Improving getAllDelivered()
This request involves a roundtrip to the database, to retrieve all orders that meet certain criteria.

Just as with `getAllProducts()`, we can free up API threads by suspending them until the lengthy internal operation completed.

On a higher level, we could utilise push-based updates of individual orders to prevent the front end from continually having to ask for all orders. This reduces the total data transmitted between frontend and backend.


Possible changes:

- Freeing up the API thread by suspending the thread while waiting.
- ORM/database optimizations
- Implementing push-based notifications to reduce number of calls

## Case 2: Adding products to order

![AddProduct](http://www.plantuml.com/plantuml/png/RPB1ReCm44JlblmF8qxD0RMzSeX2bQZAgQW97x1WgxAIW6MRAltxhi5KgO2JjJFxTjPcvmEvCDokA9MgNA-6DaZXBwQpvEY6j_vUaLAc-yBhYnHIb6te7R93fr1xxJAGny07ARWXcDPOwWBUwsCqD_qTsOTJsaL0G-q5kbEP5E3lqPzz7avdbDLnv94rIyj9ks_JQYcO00Ol-NiAr93NAtoLf7_TCJRoRGnRW9U4dVVd2WlR6AH0LP_HfndFWsKKOrY9hIbKWQd-DFesKgHwjCoZ5vvPGBu2sg2OXZwmyXHeQHdJnC3ZAYk1_vpJPkEB5L36solz9A2QFLBi-9Rud_W5 "AddProduct")

Every time the user adds a product, a roundtrip is made to the back end to synchronize the order.

This is the simplest and most straightforward implementation for the front end, but there is no hard requirement to update the order for every new product.

If the front end kept track of added products, and only called the back end to confirm the order, this would significantly cut down on the number of interactions with the back end.

The downside is that the back end can only verify that the product is in stock when the order is confirmed.

Possible changes:

- Don't update the order in back end when adding a product.

## Case 3: Confirming order

![Confirm Order](http://www.plantuml.com/plantuml/png/VLEnRi8m4Dqj-HzEcM10RLSB8MYBAgQY0biN9xvK5iQER0Vn-JsR106XpHJTlNltthja7gBqiJqOU7Esrlu0drwXvopijMsabmTuTQUrL4hR7tXvveopHHMTXs9f5Um2-W9aW3Qa8Hg8kjADj14-Tgi4Bjq9YioP24fPxG6jAZW3UgxnnTToiuN5UfNvPAE7a0twewwGCs808hxmRw8Ztd_8Cu2uXwCNQUyoob86V32jkjRDT78CardoAQ3gmdFdyAX6hLbo8M2zsvxhjEMfRasjZU6CMccwioIWQumoSioPTH71O1q5eFUKjwx1EcYyKsqL0sZRB-Ei8ybydL417adW4I-4FMWwcJn0_R4nv60znjPRL5szSWhXsyAe4qEw8iYZr4QM1iTtYlhm1BGDlJyMD02DGVJ-Kb9Pz7rKLvWK-3I1XecV2SNjQVBtIcSpACbFwptIcIzvpkdCz4ly0W00 "Confirm Order")

Orders can be refused for multiple reasons when trying to confirm them.
The order should be displayed in the front end until it is successfully confirmed (or a new order is created)

The checks here should happen as quickly as possible: if there is any reason to refuse the order, the user wants to know as soon as possible.

Two interactions with external components happen: the stock API is called to reserve products, and the order is updated in the database.

As with other requests, the API thread can be suspended until the external actions are finished.

We could possibly execute the database and Stock actions simultaneously, but that would require a rollback if one of them fails.

Possible changes:

- suspend API thread until back end actions are done
- simultaneous call to Stock API and database
