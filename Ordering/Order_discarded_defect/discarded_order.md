# Defect: Discarded Orders

The order system / API currently operates as locking state machine: any MAKEABLE / DELIVERABLE orders that are requested, are automatically marked as MAKING / DELIVERING.

Currently, there is no mechanism in place to reset this order state if the implementing agent crashed or otherwise discarded the order without completing the state.

## How to reproduce

A order can be discarded in a number of ways, some being hardware/software defects, and some requiring user actions.

### System exit

Any system exit while currently processing an order will discard the order. This can include, but is not limited to: browser crashes, browser refreshes, or the user simply exiting the application.

### Discarded request results

Currently, there are no restrictions on programmatically making multiple requests to `ordering/trymake` or `ordering/trydeliver`, and simply discarding the response. 

The front end disables the button if the previous order was not completed, but this is more a suggestion than a restriction.

## Possible fixes

Two separate issues and possible fixes can be identified here: 

- lack of assigned responsibility: the back end gives an order to anyone who asks for it, blindly trusting that whomever asks for it handles it.
- lack of oversight: the order is requested, and from the perspective of the back end then goes underwater until an update is received. The backend does not require any health checks, or periodic check-ins to confirm the order is still being processed.

This indicates that this defect by tackling either of these issues. Orders can be coupled to whomever requests it, thereby assigning responsibility. Otherwise, the backend can require frequent checkups. If they are not made, the back end automatically considers the order as discarded, and returns it to the MAKEABLE/DELIVERABLE state.