# Enterprise Integration Patterns used in Project Pub

## Message Broker

A message broker is an independent entity that can receive and distribute messages to and from a multitude of clients.
It ensures decoupling between communicating components, and efficient many-to-many messaging.

It was implemented within the Project Pub ordering environment as a way for back ends to share data with N amount of peers. Using the broker ensures that message synchronization works regardless of there being 1 or 20 back end containers running.

## Messaging Gateway

A messaging gateway is a relaying agent that decouples sender and receiver of the message. This decoupling is done to avoid making one party (or both) aware of the internals of its counterpart.

Within Project Pub, the back end is the gateway. This is done for multiple reasons:

**Drop-in software** It allowed drop-in adjustment of the messaging flow: from the perspective of the front end, nothing changed. It still received all messages through a WebSocket channel it subscribed to. The front end still only communicates HTTP API's in one location. Even the WebSocket communication is done with the same address, using basic HTML5 technology.

**Less external exposure** For the front end, it is convenient, but for security purposes it also matters that the external surface of any system is kept as small as possible. With this solution, the message broker is only ever approached internally. This gives external attackers one less opportunity to compromise the system as a whole.

**Spreading traffic** Having front end clients subscribe directly to the message broker, it would create a bottleneck in the system, where one component suddenly is communicating with all front ends.

In an environment with 1000 front ends, and 10 back ends, this would mean that all 1000 front ends subscribe to a single message broker. In the current design, 10 back ends subscribe to the message broker, and some 100 front ends subscribe to each back end. This improves performance and reliability of the whole system.

## Message Dispatcher

Scaling of back ends by simply creating more Docker containers is made possible by the load balancer. It listens to the publicly exposed port, and distributes HTTP requests evenly over all back ends.
This allows scaling the whole system based on current requirements, without having to do a (virtual) hardware migration. 

## Publish Subscribe Channel

Back end synchronization is done over a Kafka message topic, using the basic publish/subscribe mechanics. Each back end subscribes to the topic, and any backend making order updates publishes a message to the topic.
All subscribed systems are notified, and can relay the update to their (Front end) subscribers.

## Idempotent Receiver

An Idempotent receiver means that the system should be stable when receiving the same message twice.

In Project Pub, this is implemented to smooth the display of orders in the GUI. The normal flow for any connecting client is to first make a request to `ordering/alldelivered` to get all current orders that should be displayed in bulk.

After this, the list is incrementally kept up-to-date using the WebSocket subscription where changes are published. To avoid race conditions where orders are present in the GET request, but also received through the WebSocket connection, the update procotol has two event types.

`remove` tells the GUI it should remove the specified order with given ID from the list. This can safely be received multiple times, as the order ID is unique, and it will only remove the order with that ID.

`update` is used for both new orders, and updates to existing orders. The last event will be considered 'best'. Not having a separate 'create' event ensures that the system is not confused when receiving 0 or multiple identical events for the same order.