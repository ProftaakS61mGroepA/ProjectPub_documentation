## Proposal for stories implemented in Sprint 2

## User stories

**Stock products**

> To order available products, the server can view the consumptions available in stock.

**Stock reserving**

> To avoid ordering unavailable products, the server now gets immediate feedback when trying to add an unavailable (out of stock) consumption.

**Role-based availability**

> To better control functionality usage, users can now only view and access functionality relevant to their role.

**Realtime updates of delivered orders**

> To accurately display current orders, the delivered orders tab is now updated in real-time

**Create/Modify products in bulk**
> A user should be able to add/modify all products in bulk.  
> Sub-tasks:
> - Research the best way to implement this feature.  
> Results of this research should be discussed with the productowner before proceeding.
> - Implement retrieval of products
> - Add retrived products to the database

## Technical stories

**Distributed scalability**

> To improve scalability, multiple instances of the Ordering back end should run concurrently.

**Payara deployment**

> To streamline deployment, bringing the Ordering application online in a docker container should be a one-step operation.

**Async optimization**

> To improve scalability, long-running waits for external actions are now performed asynchronously.