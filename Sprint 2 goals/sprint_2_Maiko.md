## Proposal for stories implemented in Sprint 2

## User stories

**Check product availability**
> A developer should be able to check de availability of a product for use in his own application.

**Reserve a product**
> A developer should be able to reserve a product for use in his own application.

**Use a product**
> A developer should be able to signal that a reserved product has been used in his application.

**Role-based availability**

> To better control functionality usage, users can now only view and access functionality relevant to their role.

## Technical stories

**Research Security**
> Research how security should be implemented using a Role-bases system.